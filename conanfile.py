from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration

import os
import textwrap

class RmlUiConan(ConanFile):
    name = "RmlUi"
    version = "3.3"
    homepage = "https://github.com/mikke89/RmlUi"
    url = "https://gitlab.com/b110011/conan-rmlui"
    description = "RmlUi - The HTML/CSS User Interface library evolved"
    author = "The RmlUi Team"
    license = "MIT"
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    topics = ("conan", "cpp", "cpp14", "gui", "graphical", "rmlui")
    options = {
        "fPIC": [True, False],
        "shared": [True, False],
        "enable_tracy_profiling": [True, False],
        "with_default_font_interface": [True, False],
        "with_lua_bindings":  [True, False],
        "with_matrix_row_major": [True, False],
        "with_thirdparty_containers": [True, False],
        "with_rtti_and_exceptions": [True, False],
        "use_precompiled_headers": [True, False]
    }
    default_options = {
        "fPIC": True,
        "shared": False,
        "enable_tracy_profiling": False,
        "use_precompiled_headers": True,
        "with_default_font_interface": False,
        "with_lua_bindings":  False,
        "with_matrix_row_major": False,
        "with_rtti_and_exceptions": True,
        "with_thirdparty_containers": True
    }
    _build_subfolder = "build_subfolder"
    _source_subfolder = "source_subfolder"

    def source(self):
        url = "{0}/archive/{1}.tar.gz".format(self.homepage, self.version)
        sha256 = "ab24539856045b0a039066a2257a3346712c28b516a60effa2eb1ea00ed1d192"
        tools.get(url, sha256=sha256)

        extracted_dir = "{0}-{1}".format(self.name, self.version)
        os.rename(extracted_dir, self._source_subfolder)

    def requirements(self):
        if self.options.enable_tracy_profiling:
            #self.requires.add("tracy-client")
            pass

        if self.options.with_default_font_interface:
            self.requires.add("freetype/2.10.0@bincrafters/stable")

        if self.options.with_lua_bindings:
            self.requires.add("lua/5.3.5")

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.remove("fPIC")

    def _check_cpp14_support(self):
        supported_compilers = [("gcc", "5"), ("clang", "3.2"), ("apple-clang", "4.3"), ("Visual Studio", "14")]

        compiler = self.settings.compiler
        version = tools.Version(compiler.version)

        return any(compiler == sc[0] and version >= sc[1] for sc in supported_compilers)

    def configure(self):
        if self.settings.compiler.get_safe("cppstd"):
            tools.check_min_cppstd(self, 14)
        elif not self._check_cpp14_support():
            raise ConanInvalidConfiguration("Compiler is too old to build RmlUi")

    def _configure_cmake(self):
        cmake = CMake(self)

        cmake.definitions["BUILD_SAMPLES"] = False
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared
        #cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["DISABLE_RTTI_AND_EXCEPTIONS"] = not self.options.with_rtti_and_exceptions
        cmake.definitions["ENABLE_PRECOMPILED_HEADERS"] = self.options.use_precompiled_headers
        cmake.definitions["MATRIX_ROW_MAJOR"] = self.options.with_matrix_row_major
        cmake.definitions["NO_THIRDPARTY_CONTAINERS"] = not self.options.with_thirdparty_containers

        cmake.definitions["ENABLE_TRACY_PROFILING"] = self.options.enable_tracy_profiling
        if self.options.enable_tracy_profiling:
            #cmake.definitions["TRACY_DIR"] = self.deps_cpp_info['tracy'].rootpath
            pass

        cmake.definitions["NO_FONT_INTERFACE_DEFAULT"] = not self.options.with_default_font_interface
        if self.options.with_default_font_interface:
            cmake.definitions["FREETYPE_DIR"] = self.deps_cpp_info['freetype'].rootpath

        cmake.definitions["BUILD_LUA_BINDINGS"] = self.options.with_lua_bindings
        if self.options.with_lua_bindings:
            cmake.definitions["LUA_DIR"] = self.deps_cpp_info['lua'].rootpath

        cmake.configure(source_folder=self._source_subfolder, build_folder=self._build_subfolder)

        return cmake

    def _patch(self):
        tools.replace_in_file(os.path.join(self._source_subfolder, "CMakeLists.txt"), 'set(RMLUI_VERSION_RELEASE true)', """
set(RMLUI_VERSION_RELEASE true)

include("${CMAKE_SOURCE_DIR}/../conanbuildinfo.cmake")
conan_basic_setup()
""")

    def build(self):
        self._patch()
        cmake = self._configure_cmake()
        cmake.build()

    def package(self):
        self.copy("LICENSE.txt", dst="licenses", src=self._source_subfolder)
        self.copy(pattern="Samples/basic/sdl2/src/*SDL2.*", dst="misc/bindings", src=self._source_subfolder, keep_path=False)
        self.copy(pattern="Samples/basic/sfml2/src/*SFML.*", dst="misc/bindings", src=self._source_subfolder, keep_path=False)

        self._create_cmake_module_variables(
            os.path.join(self.package_folder, self._module_file_rel_path)
        )

        cmake = self._configure_cmake()
        cmake.install()

    @staticmethod
    def _create_cmake_module_variables(module_file):
        content = textwrap.dedent("""\
            if(DEFINED RmlUi_FOUND)
                set(RMLUI_FOUND ${RmlUi_FOUND})
            endif()
            if(DEFINED RmlUi_INCLUDE_DIR)
                set(RMLUI_INCLUDE_DIR ${RmlUi_INCLUDE_DIR})
            endif()
            if(DEFINED RmlUi_Core_LIBS)
                set(RMLUI_CORE_LIBRARY ${RmlUi_Core_LIBS})
                set(RMLUI_CORE_LIBRARIES ${RmlUi_Core_LIBS}
                                         ${RmlUi_Core_DEPENDENCIES}
                                         ${RmlUi_Core_FRAMEWORKS}
                                         ${RmlUi_Core_SYSTEM_LIBS})
            endif()
            if(DEFINED RmlUi_Controls_LIBS)
                set(RMLUI_CONTROLS_LIBRARY ${RmlUi_Controls_LIBS})
                set(RMLUI_CONTROLS_LIBRARIES ${RmlUi_Controls_LIBS}
                                             ${RmlUi_Controls_DEPENDENCIES}
                                             ${RmlUi_Controls_FRAMEWORKS}
                                             ${RmlUi_Controls_SYSTEM_LIBS})
            endif()
            if(DEFINED RmlUi_Debugger_LIBS)
                set(RMLUI_DEBBUGER_LIBRARY ${RmlUi_Debugger_LIBS})
                set(RMLUI_DEBBUGER_LIBRARIES ${RmlUi_Debugger_LIBS}
                                             ${RmlUi_Debugger_DEPENDENCIES}
                                             ${RmlUi_Debugger_FRAMEWORKS}
                                             ${RmlUi_Debugger_SYSTEM_LIBS})
            endif()
            if(DEFINED RmlUi_CoreLua_LIBS)
                set(RMLUI_CORELUA_LIBRARY ${RmlUi_CoreLua_LIBS})
                set(RMLUI_CORELUA_LIBRARY ${RmlUi_CoreLua_LIBS}
                                          ${RmlUi_CoreLua_DEPENDENCIES}
                                          ${RmlUi_CoreLua_FRAMEWORKS}
                                          ${RmlUi_CoreLua_SYSTEM_LIBS})
            endif()
            if(DEFINED RmlUi_ControlsLua_LIBS)
                set(RMLUI_CONTROLSLUA_LIBRARY ${RmlUi_ControlsLua_LIBS})
                set(RMLUI_CONTROLSLUA_LIBRARY ${RmlUi_ControlsLua_LIBS}
                                              ${RmlUi_ControlsLua_DEPENDENCIES}
                                              ${RmlUi_ControlsLua_FRAMEWORKS}
                                              ${RmlUi_ControlsLua_SYSTEM_LIBS})
            endif()
            if(DEFINED RmlUi_LIBRARIES)
                set(RMLUI_LIBRARIES ${RmlUi_LIBRARIES})
            endif()
            if(DEFINED RmlUi_VERSION)
                set(RMLUI_VERSION ${RmlUi_VERSION})
            endif()
        """)
        tools.save(module_file, content)

    @property
    def _module_subfolder(self):
        return os.path.join("lib", "RmlUi", "cmake")

    @property
    def _module_file_rel_path(self):
        return os.path.join(self._module_subfolder, "{}Variables.cmake".format(self.name))

    def package_info(self):
        self.cpp_info.names["cmake_find_package"] = "RmlUi"
        self.cpp_info.names["cmake_find_package_multi"] = "RmlUi"

        core_component = self.cpp_info.components["core"]
        core_component.builddirs.append(self._module_subfolder)
        core_component.build_modules["cmake_find_package"] = [self._module_file_rel_path]
        core_component.libs = ["RmlCore"]
        core_component.names["cmake_find_package"] = "Core"
        core_component.names["cmake_find_package_multi"] = "Core"
        core_component.names["pkg_config"] = "libRmlCore"

        if not self.options.shared:
            core_component.defines.append('RMLUI_STATIC_LIB')

        if self.options.with_default_font_interface:
            core_component.requires = ["freetype::freetype"]

        if self.options.with_matrix_row_major:
            core_component.defines.append("RMLUI_MATRIX_ROW_MAJOR")

        if not self.options.with_thirdparty_containers:
            core_component.defines.append("RMLUI_NO_THIRDPARTY_CONTAINERS")

        if self.options.enable_tracy_profiling:
            core_component.defines.append("RMLUI_ENABLE_PROFILING")

        if not self.options.with_rtti_and_exceptions:
            core_component.defines.append("RMLUI_USE_CUSTOM_RTTI")

        control_component = self.cpp_info.components["controls"]
        control_component.builddirs.append(self._module_subfolder)
        control_component.build_modules["cmake_find_package"] = [self._module_file_rel_path]
        control_component.libs = ["RmlControls"]
        control_component.names["cmake_find_package"] = "Controls"
        control_component.names["cmake_find_package_multi"] = "Controls"
        control_component.names["pkg_config"] = "libRmlControls"
        control_component.requires = ["core"]

        debugger_component = self.cpp_info.components["debugger"]
        debugger_component.builddirs.append(self._module_subfolder)
        debugger_component.build_modules["cmake_find_package"] = [self._module_file_rel_path]
        debugger_component.libs = ["RmlDebugger"]
        debugger_component.names["cmake_find_package"] = "Debugger"
        debugger_component.names["cmake_find_package_multi"] = "Debugger"
        debugger_component.names["pkg_config"] = "libRmlDebugger"
        debugger_component.requires = ["core"]

        if self.options.with_lua_bindings:
            core_lua_component = self.cpp_info.components["core_lua"]
            core_lua_component.builddirs.append(self._module_subfolder)
            core_lua_component.build_modules["cmake_find_package"] = [self._module_file_rel_path]
            core_lua_component.libs = ["RmlCoreLua"]
            core_lua_component.names["cmake_find_package"] = "CoreLua"
            core_lua_component.names["cmake_find_package_multi"] = "CoreLua"
            core_lua_component.names["pkg_config"] = "libRmlCoreLua"
            core_lua_component.requires = ["core", "lua::lua"]

            controls_lua_component = self.cpp_info.components["controls_lua"]
            controls_lua_component.builddirs.append(self._module_subfolder)
            controls_lua_component.build_modules["cmake_find_package"] = [self._module_file_rel_path]
            controls_lua_component.libs = ["RmlControlsLua"]
            controls_lua_component.names["cmake_find_package"] = "ControlsLua"
            controls_lua_component.names["cmake_find_package_multi"] = "ControlsLua"
            controls_lua_component.names["pkg_config"] = "libRmlControlsLua"
            controls_lua_component.requires = ["controls", "core_lua", "lua::lua"]
